import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client'

@Injectable()
export class UserService {
    prisma = new PrismaClient();
    async getUser(){
        let data = await this.prisma.hr_user.findMany();
        console.log("nguoi dung:",data);
        return {
            message: "thanh cong!",
            data
        }
    }
}
